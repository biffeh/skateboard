<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test

Route::get('/', 'PostController@home');
Route::get('/naujas', 'PostController@forma');
Route::post('/post','PostController@saugok');
Route::get('/','PublicPostController@paiimk');
Route::get('/login', '\App\Http\Controllers\Auth\LoginController@login');
Route::get('/register', '\App\Http\Controllers\Auth\LoginController@register');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/post/{post}','PublicPostController@showPost');
Route::get('/admin','PostController@admin');
//Route::get('/{category}/','PublicPostController@showCat')->where('{category}', 'category');
Route::get('/{cats}/','PublicPostController@showCat');




Route::get('/post/{post}/edit','PostController@editPost');
Route::patch('/post/{post}/','PostController@updatePost');
Route::get('/post/{post}/delete','PostController@deletePost');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');





