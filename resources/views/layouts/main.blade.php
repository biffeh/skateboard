<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
@include('inc/head')
<body>

@include('inc/nav')

<!-- Main jumbotron for a primary marketing message or call to action -->
@include('inc/header')

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        @yield('content')
    </div>

    <hr>

    @include('inc/footer')
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>
<script src="/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
