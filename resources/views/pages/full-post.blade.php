@extends ('layouts/main')
@section ('content')


    <h2>{{$post->title}}</h2>
    <h3>{{$post->category}}</h3>
    <p>{{$post->body}}</p>



    <hr>
    <div class="cards">
        <div class="card-block">
            <a href="/">Grįžti namo</a>
        </div>
    </div>
    @if(Auth::id() == $post->user_id)
        <p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Redaguoti</a></p>
        <p><a class="btn btn-danger" href="/post/{{$post->id}}/delete" role="button">Trinti</a></p>
    @endif
@endsection

