
@extends('layouts/main')
@section('content')

    <h2>Riedlenčių sąrašas</h2>
    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Category</th>
            <th>Author</th>
            <th>Content</th>
            <th>Buttons</th>
        </tr>
        @foreach($post as $postkey)
            <tr>
                <td>{{$postkey->title}}</td>
                <td>{{$postkey->category}}</td>
                <td>{{$postkey->name}}</td>
                <td><p>{{str_limit($postkey->body, 100)}}</p></td><br>
                <td><a class="btn btn-default" href="/post/{{$postkey->id}}" role="button">Rodyti visą informaciją</a>
                    <a class="btn btn-default" href="/post/{{$postkey->id}}/edit" role="button">Redaguoti</a>
                    <a class="btn btn-danger" onclick="return confirm('Ar tikrai norite istrinti?')" href="/post/{{$postkey->id}}/delete" role="button">Trinti</a></td>
            </tr>

        @endforeach
    </table>
    <a class="btn btn-success" href="/naujas" role="button">Sukurti naują skelbimą</a>
    <a href="/">Grįžti namo</a>
    {{--{{$posts->links()}}--}}
@endsection
